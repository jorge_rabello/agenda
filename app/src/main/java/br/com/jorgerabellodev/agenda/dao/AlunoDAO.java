package br.com.jorgerabellodev.agenda.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.jorgerabellodev.agenda.modelo.Aluno;

/**
 * Esta classe deve representado um opbjeto DAO
 * para o pbjeto Aluno
 *
 * @author jorge.rabello
 */
public class AlunoDAO extends SQLiteOpenHelper {


    private static final String DB_NAME = "Agenda";
    private static final int DB_VERSION = 2;

    public AlunoDAO(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS aluno (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "nome TEXT NOT NULL, " +
                "endereco TEXT NOT NULL, " +
                "telefone TEXT NOT NULL, " +
                "site TEXT, " +
                "nota REAL, " +
                "caminhoFoto TEXT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        switch (oldVersion) {
            case 1:
                String sql = "ALTER TABLE aluno ADD COLUMN caminhoFoto TEXT";
                db.execSQL(sql);
                break;
        }
    }

    /**
     * Persiste um aluno na base de dados
     *
     * @param aluno aluno a ser persistido na base de dados.
     */
    public void persistir(Aluno aluno) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = pegaDadosDoAluno(aluno);

        db.insert("aluno", null, values);
    }

    /**
     * Recupera todos os alunos cadastrados
     *
     * @return lista de alunos cadastrados no banco de dados.
     */
    public List<Aluno> buscaAlunos() {
        String sql = "SELECT * FROM aluno;";
        SQLiteDatabase db = getReadableDatabase();

        List<Aluno> alunos = new ArrayList<>();

        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            Aluno aluno = new Aluno();
            aluno.setId(cursor.getLong(cursor.getColumnIndex("id")));
            aluno.setNome(cursor.getString(cursor.getColumnIndex("nome")));
            aluno.setEndereco(cursor.getString(cursor.getColumnIndex("endereco")));
            aluno.setTelefone(cursor.getString(cursor.getColumnIndex("telefone")));
            aluno.setSite(cursor.getString(cursor.getColumnIndex("site")));
            aluno.setNota(cursor.getDouble(cursor.getColumnIndex("nota")));
            aluno.setFoto(cursor.getString(cursor.getColumnIndex("caminhoFoto")));
            alunos.add(aluno);
        }
        cursor.close();

        return alunos;
    }

    /**
     * Exclui um aluno da base de dados.
     *
     * @param aluno aluno a ser excluído da base de dados.
     */
    public void excluir(Aluno aluno) {
        SQLiteDatabase db = getWritableDatabase();
        String[] params = {aluno.getId().toString()};
        db.delete("aluno", "id = ?", params);
    }

    public void altera(Aluno aluno) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = pegaDadosDoAluno(aluno);

        String[] params = {String.valueOf(aluno.getId())};

        db.update("aluno", values, "id = ?", params);

    }

    /**
     * Verifica se um aluno está cadastrado na base de dados.
     *
     * @param telefone chave para consultar o aluno.
     * @return verdadeiro caso o aluno esteja cadastrado e falso caso não.
     */
    public boolean isAluno(String telefone) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM aluno WHERE telefone = ?", new String[]{telefone});
        int resultados = cursor.getCount();
        cursor.close();
        return resultados > 0;
    }

    @NonNull
    private ContentValues pegaDadosDoAluno(Aluno aluno) {
        ContentValues values = new ContentValues();
        values.put("nome", aluno.getNome());
        values.put("endereco", aluno.getEndereco());
        values.put("telefone", aluno.getTelefone());
        values.put("site", aluno.getSite());
        values.put("nota", aluno.getNota());
        values.put("caminhoFoto", aluno.getFoto());
        return values;
    }
}
