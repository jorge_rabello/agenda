package br.com.jorgerabellodev.agenda.provider;

import android.support.v4.content.FileProvider;

/**
 * Provider para armazenar a foto no armazenamento interno.
 *
 * @author jorge.rabello
 */
public class GenericFileProvider extends FileProvider {
}
