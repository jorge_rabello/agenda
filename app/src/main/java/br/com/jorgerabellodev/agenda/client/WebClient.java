package br.com.jorgerabellodev.agenda.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Esta classe executa operaões de rede
 *
 * @author jorge.rabello
 */
public class WebClient {

    /**
     * Executa um post para um url específica
     *
     * @param json dados para enviar ao servidor
     * @return resposta do servidor
     */
    public String post(String json) {
        try {

            URL url = new URL("https://www.caelum.com.br/mobile");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");


            // indica que vamos escrever na saída padrão da conexão (vamos fazer um post !)
            connection.setDoOutput(true);
            PrintStream output = new PrintStream(connection.getOutputStream());

            // escreve o json no body da requisição
            output.println(json);

            // se conecta ao servidor
            connection.connect();

            // pega a resposta do servidor
            Scanner scanner = new Scanner(connection.getInputStream());

            // se tudo deu certo retorna a resposta
            return scanner.next();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // em caso de problemas retorna null
        return null;
    }
}
