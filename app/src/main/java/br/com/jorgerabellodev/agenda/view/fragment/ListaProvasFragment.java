package br.com.jorgerabellodev.agenda.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import br.com.jorgerabellodev.agenda.R;
import br.com.jorgerabellodev.agenda.modelo.Prova;
import br.com.jorgerabellodev.agenda.view.activity.DetalhesProvaActivity;
import br.com.jorgerabellodev.agenda.view.activity.ProvasActivity;

public class ListaProvasFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista_provas, container, false);


        List<String> topicosPort = Arrays.asList("Sujeito", "Objeto direto", "Objeto indireto", "Artigo");
        Prova provaDePortugues = new Prova("Português", "25/05/2016", topicosPort);

        List<String> topicosMat = Arrays.asList("Equações de segundo grau", "Trigonometria");
        Prova provaDeMatematica = new Prova("Matemática", "27/05/2016", topicosMat);

        List<Prova> provas = Arrays.asList(provaDePortugues, provaDeMatematica);

        ArrayAdapter<Prova> adapter = new ArrayAdapter<Prova>(getContext(), android.R.layout.simple_list_item_1, provas);

        ListView lista = view.findViewById(R.id.provas_lista);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Prova prova = (Prova) parent.getItemAtPosition(position);
                Toast.makeText(getContext(), "Clocou na prova de " + prova, Toast.LENGTH_SHORT).show();

                ProvasActivity activity = (ProvasActivity) getActivity();
                activity.selecionaProva(prova);

            }
        });

        return view;
    }
}
