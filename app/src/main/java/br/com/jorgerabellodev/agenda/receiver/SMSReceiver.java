package br.com.jorgerabellodev.agenda.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.telephony.SmsMessage;
import android.widget.Toast;

import br.com.jorgerabellodev.agenda.R;
import br.com.jorgerabellodev.agenda.dao.AlunoDAO;

/**
 * Broadcast receiver para tratar SMS.
 *
 * @author jorge.rabello
 */
public class SMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // recupera todas as pdus
        Object[] pdus = (Object[]) intent.getSerializableExtra("pdus");

        // pega a primeira pdu
        byte[] pdu = (byte[]) pdus[0];

        // obtem o formato da pdu
        String formato = (String) intent.getSerializableExtra("format");

        // cria o sms
        SmsMessage sms;

        // preenche o sms a partir da pdu
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            sms = SmsMessage.createFromPdu(pdu, formato);
        } else {
            sms = SmsMessage.createFromPdu(pdu);
        }
        // pega o telefone que enviou o sms
        String telefone = sms.getDisplayOriginatingAddress();

        // verifica se o telefone pertence a um dos alunos cadastrados na base de dados
        AlunoDAO dao = new AlunoDAO(context);
        if (dao.isAluno(telefone)) {
            // se sim exibe um toast indicando que chegou um sms de aluno
            Toast.makeText(context, "Chegou um SMS de Aluno !", Toast.LENGTH_SHORT).show();
            // toca uma música diferente para o sms de aluno
            MediaPlayer mp = MediaPlayer.create(context, R.raw.msg);
            mp.start();
        }
        dao.close();
    }
}
