package br.com.jorgerabellodev.agenda.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.List;

import br.com.jorgerabellodev.agenda.client.WebClient;
import br.com.jorgerabellodev.agenda.converter.AlunoConverter;
import br.com.jorgerabellodev.agenda.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.modelo.Aluno;

/**
 * Esta classe representa uma async task para comunicação com o WebService
 *
 * @author jorge.rabello
 */
public class EnviarAlunosTask extends AsyncTask<Void, Void, String> {

    private Context contexto;
    private ProgressDialog dialog;

    public EnviarAlunosTask(Context contexto) {
        this.contexto = contexto;
    }

    // acontece antes do doInBackground
    @Override
    protected void onPreExecute() {
        // exibe uma janela de diálogo
        dialog = ProgressDialog.show(contexto, "Aguarde...", "Enviando alunos...", true, true);
    }

    @Override
    protected String doInBackground(Void... params) {
        AlunoDAO dao = new AlunoDAO(contexto);
        List<Aluno> alunos = dao.buscaAlunos();
        dao.close();

        AlunoConverter conversor = new AlunoConverter();
        String json = conversor.converterParaJSON(alunos);

        WebClient client = new WebClient();

        return client.post(json);

    }


    // é executado após o doInBackground
    @Override
    protected void onPostExecute(String resposta) {
        // encerra a progress bar
        dialog.dismiss();
        Toast.makeText(contexto, resposta, Toast.LENGTH_SHORT).show();
    }
}
