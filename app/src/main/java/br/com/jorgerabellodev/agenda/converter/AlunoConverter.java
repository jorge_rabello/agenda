package br.com.jorgerabellodev.agenda.converter;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

import br.com.jorgerabellodev.agenda.modelo.Aluno;

/**
 * Esta classe deve atuar como conversor de objetos para json
 *
 * @author jorge.rabello
 */
public class AlunoConverter {

    /**
     * converte uma lista de alunos para json
     *
     * @param alunos lista a ser convertida para json
     * @return objeto json resultante
     */
    public String converterParaJSON(List<Aluno> alunos) {

        JSONStringer js = new JSONStringer();

        try {

            js.object()
                    .key("list")
                    .array()
                    .object()
                    .key("aluno")
                    .array();

            for (Aluno aluno : alunos) {
                js.object();
                js.key("nome").value(aluno.getNome());
                js.key("nota").value(aluno.getNota());
                js.endObject();
            }

            js.endArray()
                    .endObject()
                    .endArray()
                    .endObject();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js.toString();
    }

}
